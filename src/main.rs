// Copyright (C) 2022 Maxime “pep” Buquet <pep@bouah.net>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

pub mod mm;
use mm::modem::Modem as ModemAccess;

use std::fmt;
use std::sync::Arc;

use futures_util::stream::StreamExt;
use tokio::{
    task::JoinHandle,
    time::Duration,
};

use mmdbus::{
    dbus::{
        message::MatchRule,
        nonblock::{
            SyncConnection, Proxy,
            stdintf::org_freedesktop_dbus::ObjectManager,
        },
        strings::{Path as DBusPath},
        Error as DBusError,
    },
};

use dbus_tokio::connection;

// The following source has been used as an example:
// https://github.com/soerenmeier/linux-info/blob/master/src/network/modem_manager.rs

const MM_NAME: &str = "org.freedesktop.ModemManager1";
const MM_PATH: &str = "/org/freedesktop/ModemManager1";
const TIMEOUT: Duration = Duration::from_secs(5);

#[derive(Clone)]
pub struct DBus {
    pub conn: Arc<SyncConnection>,
    pub handle: Arc<JoinHandle<()>>,
}

impl DBus {
    pub fn connect() -> Result<Self, DBusError> {
        let (resource, conn) = connection::new_system_sync()?;

        let handle: JoinHandle<()> = tokio::spawn(async {
            let err = resource.await;
            panic!("Lost connection to D-Bus: {}", err);
        });

        Ok(Self { conn, handle: Arc::new(handle) })
    }

    pub fn proxy<'a, 'b, P>(&'b self, path: P) -> Proxy<'a, &'b SyncConnection>
    where
        P: Into<DBusPath<'a>>,
    {
        Proxy::new(MM_NAME, path, TIMEOUT, &*self.conn)
    }
}

#[derive(Clone)]
pub struct ModemManager {
    dbus: DBus,
}

impl ModemManager {
    pub fn connect() -> Result<Self, DBusError> {
        DBus::connect().map(|dbus| Self { dbus })
    }

    pub async fn modems(&self) -> Result<Vec<Modem>, DBusError> {
        let objects = self.dbus.proxy(MM_PATH).get_managed_objects().await?;
        let modems = objects
            .into_iter()
            .map(|(path, _)| {
                Modem {
                    dbus: self.dbus.clone(),
                    path,
                }
            })
            .collect();

        Ok(modems)
    }
}

#[derive(Clone)]
pub struct Modem {
    dbus: DBus,
    pub path: DBusPath<'static>,
}

impl fmt::Debug for Modem {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Modem {{ {}, {:?}, .. }}", self.path, "1234")
    }
}

impl Modem {
    pub async fn enabled(&self) -> Result<bool, DBusError> {
        let state: ModemState = ModemAccess::state(&self.dbus.proxy(&self.path)).await.map(Into::into)?;
        Ok(match state {
            ModemState::Enabling
            | ModemState::Enabled
            | ModemState::Searching
            | ModemState::Registered
            | ModemState::Connecting
            | ModemState::Connected => true,
            _ => false,
        })
    }

    pub async fn model(&self) -> Result<String, DBusError> {
        ModemAccess::model(&self.dbus.proxy(&self.path)).await
    }
}

#[repr(i32)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ModemState {
    /// The modem is unusable.
    Failed = -1,
    /// State unknown or not reportable.
    Unknown = 0,
    /// The modem is currently being initialized.
    Initializing = 1,
    /// The modem needs to be unlocked.
    Locked = 2,
    /// The modem is not enabled and is powered down.
    Disabled = 3,
    /// The modem is currently transitioning to the MM_MODEM_STATE_DISABLED
    /// state.
    Disabling = 4,
    /// The modem is currently transitioning to the MM_MODEM_STATE_ENABLED
    /// state.
    Enabling = 5,
    /// The modem is enabled and powered on but not registered with a network
    /// provider and not available for data connections.
    Enabled = 6,
    /// The modem is searching for a network provider to register with.
    Searching = 7,
    /// The modem is registered with a network provider, and data connections
    /// and messaging may be available for use.
    Registered = 8,
    /// The modem is disconnecting and deactivating the last active packet data
    /// bearer. This state will not be entered if more than one packet data
    /// bearer is active and one of the active bearers is deactivated.
    Disconnecting = 9,
    /// The modem is activating and connecting the first packet data bearer.
    /// Subsequent bearer activations when another bearer is already active
    /// do not cause this state to be entered.
    Connecting = 10,
    /// One or more packet data bearers is active and connected.
    Connected = 11,
}

impl From<i32> for ModemState {
    fn from(num: i32) -> Self {
        if num < -1 || num > 11 {
            Self::Unknown
        } else {
            unsafe { *(&num as *const i32 as *const Self) }
        }
    }
}

async fn print_foo(mm: ModemManager) -> Result<(), DBusError> {
    let modems = mm.modems().await?;
    println!("Modems: {:?}", modems);
    for modem in modems {
        println!("Modem: {:?}", modem);
        println!("Enabled: {:?}", modem.enabled().await);
        println!("Model: {:?}", modem.model().await);
    }
    Ok(())
}

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let dbus = DBus::connect()?;

    let mut interval = tokio::time::interval(Duration::from_secs(2));

    let calls = async {
        loop {
            let mm = ModemManager { dbus: dbus.clone() };
            interval.tick().await;
            let _ = print_foo(mm).await;
        }
    };

    let mr = MatchRule::new_signal("org.freedesktop.DBus.Properties", "PropertiesChanged");

    let (_in_signal, stream) = dbus.conn.add_match(mr).await?.stream();
    let stream = stream.for_each(|(foo, (source,)): (_, (String,))| {
        println!("Foo {}; {:?}", source, foo);
        async {}
    });

    stream.await;

    futures::join!(stream, calls);

    Ok(())
}
