// Copyright (C) 2022 Maxime “pep” Buquet <pep@bouah.net>
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use std::ops::Deref;
use std::marker::Sync;

use async_trait::async_trait;
use mmdbus::dbus::{nonblock, Error as DBusError};

#[async_trait]
pub trait Modem {
    async fn enable(&self, enable: bool) -> Result<(), DBusError>;
    async fn state(&self) -> Result<i32, DBusError>;
    async fn model(&self) -> Result<String, DBusError>;
}

#[async_trait]
impl<'a, T: nonblock::NonblockReply, C: Deref<Target=T> + Sync> Modem for nonblock::Proxy<'a, C> {
    async fn enable(&self, enable: bool) -> Result<(), DBusError> {
        self.method_call("org.freedesktop.ModemManager1.Modem", "Enable", (enable,)).await
    }

    async fn state(&self) -> Result<i32, DBusError> {
        <Self as nonblock::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.freedesktop.ModemManager1.Modem", "State").await
    }

    async fn model(&self) -> Result<String, DBusError> {
        <Self as nonblock::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.freedesktop.ModemManager1.Modem", "Model").await
    }
}
