# Gateway ModemManager <-> XMPP

Bridge ModemManager SMSs and Calls to XMPP.

SPDX: AGPL-3.0-or-later

## TODO

- [ ] ModemManager
  - [ ] Modems
    - [ ] Settle on the first enabled modem.
    - [ ] Pick modem from configuration, enable it with provided string.
  - [ ] SMS
    - [ ] Monitor / Read
    - [ ] Send
  - [ ] Calls
    - [ ] Monitor / Answer
    - [ ] Make calls
- [ ] XMPP
  - …
